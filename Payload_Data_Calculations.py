"""Payload_Data_Calculations function definition."""

import math
import matplotlib.pyplot as plt
import numpy as np


def Payload_Data_Calculations(Mission_Duration, Pass_Comms_Window, Av_passes,
                              Total_days):
    """Payload_Data_Calculations function calculates the data rate.

    It takes the total mission duration in seconds and the AVERAGE
    communication window as it is calculated from another function and
    plots some diagrams.It calculates the Data rate as a function of the ratio
    of the comms window that can be used from the S band and the average photos
    per pass that can be downlinked from the satelite to the ground station
    :param Mission_Duration: Total seconds that the satelite is in contact
    with the satelite
    :type Mission_Duration: float
    :param Pass_Communication_Window: Average total seconds per  pass that
    the satelite is in contact with the satelite
    :type Pass_Communication_Window:float
    """
    # SU data.
    by = 8
    min = 60
    k = 1000
    N_Of_Exp_Runs = 1
    Hours = 72
    Time_Interval = 20
    Width = 1280
    Height = 1024
    Depth = 8
    Channels = 3
    N_Of_Micros = 1
    Comp_Ratio = 2
    Payload_Data_Packet_Length = 5.12
    Payload_Data_Packet_Field_Length = 3.888
    # printing the data and Ratio definition and assignment
    print("Ratio, is the ratio of the duration of the communication window that")
    print("is used for the S band downlink over the total duration of the window.")
    print("So the Ratio is a float number between 0 and 1. If the S band is using")
    print("the window for 80% of time we set Ratio=0.8")
    Ratio = float(input("Give the Ratio:"))
    print("\n\t The data rate will be calculated with these data:\n")
    print("Payload_Data_Packet_Length:"+str(Payload_Data_Packet_Length)+"bits")
    print("Payload_Data_Field_Length:"+str(Payload_Data_Packet_Field_Length)+"bits")
    print("Mission_Duration:"+str(Mission_Duration)+"seconds")
    print("Pass_Communication_Window:"+str(Pass_Comms_Window)+"s")
    print("Number of experiments per run:"+str(N_Of_Exp_Runs))
    print("Number of hours:"+str(Hours))
    print("Time_Interval:"+str(Time_Interval)+"min")
    print("Width:"+str(Width))
    print("Height:"+str(Height))
    print("Depth:"+str(Depth))
    print("Channels:"+str(Channels))
    print("Number of microscopes:"+str(N_Of_Micros))
    print("Compression_Ratio:"+str(Comp_Ratio))
    print("Ratio of time available for S-band downlink:"+str(Ratio))
    print("\n\t Then we calculate these data:\n")
    # Calculation for the data to be transmitted
    N_Of_Photos_Per_Micro_Per_Run = Hours*min/Time_Interval
    Size_Per_Photo_MB = Width*Height*Depth*Channels/(8*k*k)
    Size_Per_Comp_Photo_MB = Size_Per_Photo_MB/Comp_Ratio
    N_Photos_Per_run = N_Of_Photos_Per_Micro_Per_Run*N_Of_Micros
    Total_Size_Uncomp_Per_Run_GB = Size_Per_Photo_MB*N_Photos_Per_run/k
    Total_Size_Comp_GB = Size_Per_Comp_Photo_MB*N_Photos_Per_run/k
    Total_Size_Comp_All_Runs_GB = Total_Size_Comp_GB*N_Of_Exp_Runs
    Total_Size_Comp_All_Runs_Kb = Total_Size_Comp_All_Runs_GB*by*k*k
    D = Payload_Data_Packet_Length/Payload_Data_Packet_Field_Length
    Total_Size_Comp_All_Runs_Kb*D
    Total_Size_Plus_Packet_Overhead_Kb = math.floor(Total_Size_Comp_All_Runs_Kb*D)
    # Printing the calculated data
    print("Number of Images per microscope per run:"+str(N_Of_Photos_Per_Micro_Per_Run))
    print("Size per uncompressed image Width x Height x Depth x Channels/"
          "(8 x\10^6)-> MB:"+str(Size_Per_Photo_MB)+"Megabytes")
    print("Size per compressed image MB:"+str(Size_Per_Comp_Photo_MB)+"MB")
    print("Total images per run:"+str(N_Photos_Per_run)+"photos")
    print("Size of compressed image:"+str(Size_Per_Comp_Photo_MB)+"MB")
    print("Total size of uncompressed images per run:"+str(Total_Size_Uncomp_Per_Run_GB)+"GB")
    print("Total size of compressed image:"+str(Total_Size_Comp_GB)+"GB")
    print("Total size of all compressed images (per run):"+str(Total_Size_Comp_All_Runs_GB)+"GB")
    print("Total size of all compressed images (all runs):"+str(Total_Size_Comp_All_Runs_Kb)+"kbits")
    print("Total size of all runs +packet overhead:" + str(Total_Size_Plus_Packet_Overhead_Kb)+"kbits")

    data_rate = []
    window = []
    days = []
    ra = []
    images = []
    z = []
    x = []
    y = []
    print(Total_days)
    Stop = math.floor(Total_days*0.17)
    Start = 0
    A = Total_days/20
    Step = math.floor(A)
    g = 0
    print("\nThe simulation will run for this time phase, with the downtime running from 0% to 17%")
    print("and the Ratio running from 80% to 100%")
    for Days_Not_Available in range(int(Start), int(Stop), Step):
        days.append(Days_Not_Available)
        data_rate.clear()
        window.clear()
        images.clear()
        ra.clear()
        g = g+1
        for r in np.arange(Ratio, 1, 0.01):
            Real_Comms_Window = (Mission_Duration-Days_Not_Available*Pass_Comms_Window*Av_passes)*r
            window.append(Real_Comms_Window)
            Data_Rate = Total_Size_Plus_Packet_Overhead_Kb/Real_Comms_Window
            data_rate.append(Data_Rate)
            ra.append(float(r))
            Pass_Data_Transmitted = Data_Rate*Pass_Comms_Window
            Packets_Per_Run = math.floor(Pass_Data_Transmitted/5.12)
            Pass_Data_Transmitted = Packets_Per_Run*3.888
            Average_photos_per_pass = Pass_Data_Transmitted/(Size_Per_Comp_Photo_MB*by*k)
            images.append(Average_photos_per_pass)
        z.append(data_rate.copy())
        x.append(images.copy())
        y.append(window.copy())

    fig, ax = plt.subplots()
    for k in range(g):
        ax.plot(ra, z[k], label=str(days[k]))
    ax.set_xlabel('Ratio')
    ax.set_ylabel('Data Rate')
    ax.set_title("Data Rate - Ratio plot")
    ax.legend()
    plt.grid()
    plt.show()

    fig, ax = plt.subplots()
    for k in range(g):
        ax.plot(y[0], x[k], label=str(days[k]))
    ax.set_xlabel('Seconds')
    ax.set_ylabel('Images')
    ax.set_title("Images - Seconds")
    ax.legend()
    plt.grid()
    plt.show()

    return
