
import datetime
import numpy as np
from itertools import islice

eclipse_time = []
passes = []
passes_no_eclipse = []
time_between_passes = []
filename_eclipse = './gmat/LTAN_11_Mass_max_NC.txt'
filename_data = './gmat/CL-NC-20-11-HMS.txt'
filename_passes_nec_output = './gmat/passes_nec_output.txt'
filename_passes_interval = './gmat/passes_interval.txt'
index_pass = 0
index_eclipse = 0
min_pass_duration = 90
eclipse_offset = 1500 # Time it takes for ADCS to stabilize after eclipse
eclipse_delay = 60 # Time it takes for ADCS to lose pointing

EOL = '01 Jul 2025'
E1 = EOL.split(' ')
total_passes = 0
total_passes_no_eclipse = 0

def time_format(timeframe):
    return datetime.datetime.strptime(timeframe, "%d %b %Y %H:%M:%S.%f")

def initial_format(timestamp):
    return str(timestamp.isoformat()) + " " * (27 - len(timestamp.isoformat()))

i = 0

with open(filename_eclipse, 'r') as f:
    for lines in f:
        a = lines.split(' ')
        day = a[0]
        month = a[1]
        year = a[2]
        if day == E1[0] and month == E1[1] and year == E1[2]:
            n_lines = i
            break
        i += 1

with open(filename_eclipse, 'r') as f:
    for line in f:
        try:
            if line[90:95] == "Umbra":
                eclipse_lines = [line[:24], line[28:52]]
                eclipse_time.append(eclipse_lines)
        except:
            print(lines)

with open(filename_data, 'r') as f:
    for line in f:
        lines = line.strip("\n").split("   ")
        try:
            data_lines = [lines[0], lines[1][1:]]
            passes.append(data_lines)
        except:
            print(lines)
        

necl_dur = datetime.timedelta(seconds = 0)
tot_dur = datetime.timedelta(seconds = 0)

for lines in islice(eclipse_time, n_lines):
    pass_start, pass_end = passes[index_pass]
    pass_start = time_format(pass_start)
    pass_end = time_format(pass_end)

    while True:
        eclipse_end = eclipse_time[index_eclipse][1]
        eclipse_end = time_format(eclipse_end) + datetime.timedelta(seconds = eclipse_offset)
        if time_format(eclipse_time[index_eclipse][1]) + datetime.timedelta(seconds = eclipse_offset) <= pass_start:
            index_eclipse += 1
        else:
            break

    eclipse_start = eclipse_time[index_eclipse][0]
    eclipse_start = time_format(eclipse_start) + datetime.timedelta(seconds=eclipse_delay)

    tot_dur += pass_end - pass_start
    
    total_passes += 1
    
    if eclipse_start <= pass_start and eclipse_end >= pass_end:
        pass

    elif eclipse_start <= pass_start <= eclipse_end <= pass_end:
        effective_pass_dur = pass_end - eclipse_end
        if effective_pass_dur.total_seconds() < min_pass_duration:
            index_pass += 1
            continue
        total_passes_no_eclipse += 1
        necl_dur += effective_pass_dur
        passes_no_eclipse.append([initial_format(eclipse_end), initial_format(pass_end),
                                  str((effective_pass_dur).total_seconds())])

    elif pass_start <= eclipse_start <= pass_end <= eclipse_end:
        effective_pass_dur = eclipse_start - pass_start
        if effective_pass_dur.total_seconds() < min_pass_duration:
            index_pass += 1
            continue
        total_passes_no_eclipse += 1
        necl_dur += effective_pass_dur
        passes_no_eclipse.append([initial_format(pass_start), initial_format(eclipse_start),
                                  str((eclipse_start - pass_start).total_seconds())])

    elif pass_end <= eclipse_start :
        effective_pass_dur = pass_end - pass_start
        if effective_pass_dur.total_seconds() < min_pass_duration:
            index_pass += 1
            continue
        total_passes_no_eclipse += 1
        necl_dur += effective_pass_dur
        passes_no_eclipse.append([initial_format(pass_start), initial_format(pass_end),
                                  str((pass_end - pass_start).total_seconds())])
     
    if pass_start.year == 2025 and pass_start.month == 2:
        break

    index_pass += 1


for past_pass, curr_pass in zip(passes_no_eclipse, passes_no_eclipse[1:]):
    past_pass_end, curr_pass_beg = datetime.datetime.strptime(past_pass[1][:-8], "%Y-%m-%dT%H:%M:%S"), datetime.datetime.strptime(curr_pass[0][:-8], "%Y-%m-%dT%H:%M:%S") 
    time_between_passes.append((curr_pass_beg - past_pass_end).total_seconds())
    
print("Effective communication window (sec):", necl_dur.total_seconds(),
      "\nTotal communication window (sec):", tot_dur.total_seconds(),
      "\nEffective/Total communication window (sec):", necl_dur.total_seconds()/tot_dur.total_seconds(),
      "\nTotal number of passes:", total_passes,
      "\nTotal number of passes with no eclipse:", total_passes_no_eclipse,
      "\nMin time between passes (sec):", min(time_between_passes),
      "\nMax time between passes (sec):", max(time_between_passes),
      "\nMean time between passes (sec):", sum(time_between_passes)/len(time_between_passes)
      )

with open(filename_passes_nec_output, 'w+') as f:
    for list_item in passes_no_eclipse:
        f.write('%s\n' % list_item)

with open(filename_passes_interval, 'w+') as f:
    for list_item in time_between_passes:
        f.write('%s\n' % list_item)

