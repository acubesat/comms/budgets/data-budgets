"""Break the mission in 3 parts."""


def ExperimentTimeline(f):
    """Find the days of the experiments and break the mission in 3 zones."""
    exp = []
    message = "Please insert the day, month and year of the experiment:D-M-Y"
    messageFinal = "Please insert the EOL time"
    exp.append(input(message))
    exp.append(input(message))
    exp.append(input(message))
    exp.append(input(messageFinal))

    E1 = exp[0].split(' ')
    E2 = exp[1].split(' ')
    E3 = exp[2].split(' ')
    E4 = exp[3].split(' ')
    i = 0
    for lines in f:
        # Spliting the line that we read
        a = lines.split(' ')
        print(a)
        day = a[0]
        month = a[1]
        year = a[2]
        if day == E1[0] and month == E1[1] and year == E1[2]:
            e1 = i
        elif day == E2[0] and month == E2[1] and year == E2[2]:
            e2 = i
        elif day == E3[0] and month == E3[1] and year == E3[2]:
            e3 = i
        elif day == E4[0] and month == E4[1] and year == E4[2]:
            e4 = i
        i = i + 1
    return e1, e2, e3, e4
