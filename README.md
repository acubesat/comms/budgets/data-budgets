## Introduction

These scripts are used for calculations of the Data budget, the average communication window and the data rate needed for downlinking the whole payload data of the AcubeSAT mission. The scientific payload size data is hardcoded in the script and is utilized for the needed calculations.


## Functionality and Calculations

The GMAT files containing the passes information are located in the `gmat` folder. The script asks from the user to insert the gmat file name. Additionally, it requires the 3 different datetimes that the experiments are expected to occur, as well as the datetime that we expect to have retrieved the entire payload data. Finally, it reads the expected percentage of the communication window of each pass that it is expected to be used for the S-band downlink. It calculates the average communication window for each time phase and then it runs a simulation, for each timeline phase for the given Ratio and for a downtime duarion (days that the satellite will not be able to communicate) equal to 17 % of the total phase duration and plots the results concerning the needed data rate.
